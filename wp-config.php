<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'castelum' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fQxM9,/W+S;CBV?/[:q}DI[l|,ur!ynF<Iy>}$Ln5T_Qk#d5({{(jp#9CHL2m>9Q' );
define( 'SECURE_AUTH_KEY',  'V 8D JoXZzL**QSFjdk^E%~R*kIw>Y?-m|=dRMmK5L7:F%|<uo?]&^n9#L# N<)Y' );
define( 'LOGGED_IN_KEY',    'aRg)h2CkKya$ fw+{YDOLYG@xNCt-QDr3._R,$(tF6lArKhg0{>,e>jtkm3VA$gD' );
define( 'NONCE_KEY',        '0}j_`!Kd<V#d3bhC9iu}XX%K~:Kxx`92JNfk<Ms9bjV$SM$mReHyn>K02z)ak|cp' );
define( 'AUTH_SALT',        'r3W)5`Oqcpg#aBVLC{$JP0%_#/9ypWK[`b0kMVGkS^OrC(0lY_ss/2!q=)ThqKbj' );
define( 'SECURE_AUTH_SALT', 'FK!Hyr!Vd.-x>/813D.sVQcJ/gLTTWZJ,!}/l-du$z|oV=>(~&<QpObFM0pUG?$D' );
define( 'LOGGED_IN_SALT',   'xSFFTfcHFN)m&n/(l8ZSt:C@B/.p*sH@`GD+W^dv=MBJO}qJG3=nb;/5``taRI|4' );
define( 'NONCE_SALT',       'M1[Dua[^u[4tg/P0ZCU!1Z%Dy)&PoBvff(tnnNhFV#r0G$2wjf.BxZdg|DY5zuyo' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
